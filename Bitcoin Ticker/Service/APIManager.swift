//
//  APIManager.swift
//  Bitcoin Ticker
//
//  Created by iulian david on 7/29/17.
//  Copyright © 2017 iulian david. All rights reserved.
//

import Foundation

typealias HTTPResult = (APIResult) -> Void
typealias JSON = [String: Any]

public enum APIResult {
    case Success([String: Any])
    case Error(String)
}

class APIManager {
    private let session: URLSessionProtocol
    
    init(session: URLSessionProtocol = URLSession.shared) {
        self.session = session
    }
    
    func apiCall(url: URLRequest, completion: @escaping HTTPResult) {
        
        let task = session.executeRequest(request: url) { (data, response, error) -> Void in
            let resp = response as? HTTPURLResponse
            print("Response Code: \(String(describing: resp?.statusCode))")
            
            guard error == nil else {
                completion(.Error(error!.localizedDescription))
                return
            }
            
            guard let data = data , let json = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as? JSON, json != nil else {
                completion(.Error(error?.localizedDescription ?? "Unknown Error"))
                return
            }
            completion(.Success(json!))
            
        }
        task.resume()
    }
}
