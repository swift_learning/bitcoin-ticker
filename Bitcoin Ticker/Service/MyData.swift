//
//  MyData.swift
//  Bitcoin Ticker
//
//  Created by iulian david on 7/29/17.
//  Copyright © 2017 iulian david. All rights reserved.
//

import Foundation

class MyData {
    
    private static let baseURL = "https://apiv2.bitcoinaverage.com/indices/global/ticker/BTC"
    
    private static let currencyArray = ["AUD", "BRL","CAD","CNY","EUR","GBP","HKD","IDR","ILS","INR","JPY","MXN","NOK","NZD","PLN","RON","RUB","SEK","SGD","USD","ZAR"]
    
    private static let currencySymbols = ["$", "R$", "$", "¥", "€", "£", "$", "Rp", "₪", "₹", "¥", "$", "kr", "$", "zł", "lei", "₽", "kr", "$", "$", "R"]
    
    static func getData(currency: String, completion: @escaping (Model?, String?) -> ()) {
        DispatchQueue.global(qos: .userInteractive).async {
            var data : Model?
            var errorMsg : String?
            let subject = APIManager(session: URLSession.shared)
            
            let url = URL(string: "\(baseURL)\(currency)")!
            
            let urlRequest = URLRequest(url: url)
            
            subject.apiCall(url: urlRequest, completion: { (result) in
                switch result {
                case .Success(let json):
                    data = Model(json: json, symbol: currencySymbols[currencyArray.index(of: currency)!])
                case .Error(let errorMessage):
                   errorMsg = errorMessage
                }
               
                DispatchQueue.main.async {
                    completion(data, errorMsg)
                }
            })
            
        }
    }
    
    static var data: [Any] {
        return currencyArray
    }
    
    static func data(_ row: Int) -> String {
        return currencyArray[row]
    }
    
}
