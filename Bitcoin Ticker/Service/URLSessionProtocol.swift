//
//  URLSessionProtocol.swift
//  Bitcoin Ticker
//
//  Created by iulian david on 7/29/17.
//  Copyright © 2017 iulian david. All rights reserved.
//

import Foundation

import Foundation

typealias DataTaskResult = (Data?, URLResponse?, Error?) -> ()

protocol URLSessionProtocol {
    func executeRequest(request: URLRequest, completionHandler: @escaping DataTaskResult) -> URLSessionDataTaskProtocol
}

extension URLSession: URLSessionProtocol {
    func executeRequest(request: URLRequest, completionHandler: @escaping DataTaskResult) -> URLSessionDataTaskProtocol {
        return (dataTask(with: request, completionHandler: completionHandler) as URLSessionDataTask) as URLSessionDataTaskProtocol
    }
    
}

protocol URLSessionDataTaskProtocol {
    func resume()
}

extension URLSessionDataTask: URLSessionDataTaskProtocol { }

