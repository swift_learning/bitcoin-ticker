//
//  ViewController.swift
//  Bitcoin Ticker
//
//  Created by iulian david on 7/29/17.
//  Copyright © 2017 iulian david. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
   
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    @IBOutlet weak var bitcoinPriceLabel: UILabel!
    @IBOutlet weak var currencyPicker: UIPickerView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        currencyPicker.delegate = self
        currencyPicker.dataSource = self
        getData(currencyPicker.selectedRow(inComponent: 0))
    }
    
    
}

extension ViewController: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
        
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return MyData.data.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return MyData.data(row)
    }
}

extension ViewController: UIPickerViewDelegate {
    fileprivate func getData(_ row: Int) {
        spinner.startAnimating()
        MyData.getData(currency: MyData.data(row)) { [unowned self](model, errorMessage) in
            guard let model = model else {
                self.bitcoinPriceLabel.numberOfLines = 0
                self.bitcoinPriceLabel.adjustsFontSizeToFitWidth = true
                self.bitcoinPriceLabel.text = errorMessage
                self.spinner.stopAnimating()
                return
            }
            self.bitcoinPriceLabel.numberOfLines = 0
            self.bitcoinPriceLabel.adjustsFontSizeToFitWidth = true
            self.bitcoinPriceLabel.text = "\(model.symbol) \(model.price)"
            self.spinner.stopAnimating()
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.bitcoinPriceLabel.text = "Loading..."
        getData(row)
    }
}

