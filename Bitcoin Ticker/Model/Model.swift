//
//  Model.swift
//  Bitcoin Ticker
//
//  Created by iulian david on 7/29/17.
//  Copyright © 2017 iulian david. All rights reserved.
//

import Foundation


struct Model {
    let price: Double
    let symbol: String
    
    init(json: JSON, symbol: String) {
        guard let prices = json["open"] as? JSON, let returnedPrice = prices["day"] as? Double
        else {
            self.price = 0
            self.symbol = ""
            return
        }
        self.price = returnedPrice
        self.symbol = symbol
    }
    
}
